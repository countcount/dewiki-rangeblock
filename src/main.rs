/*
Copyright (C) 2023 Count Count <countvoncount123456@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

/*
TODO:
- [BUG] reblock expiry time wrong (needs to take the time of the latest reblock)
- [FEAT] new partial blocks
*/

#![warn(
    clippy::all,
    clippy::unnecessary_unwrap,
    clippy::pedantic,
    clippy::nursery
)]
#![allow(clippy::redundant_closure_for_method_calls)]
#![allow(clippy::items_after_statements)]

use std::fmt::{Display, Write};
use std::fs::OpenOptions;
use std::path::PathBuf;
use std::str::FromStr;
use std::time::{Duration, SystemTime};
use std::{env, iter};

use anyhow::{bail, Context, Result};
use bitstring::FixedBitString;
use chrono::{TimeDelta, Utc};
use cidr::{IpCidr, IpInet, Ipv4Cidr, Ipv6Cidr};
use futures::stream::TryStreamExt;
use futures::FutureExt;
use itertools::Itertools;
use mwapi_responses::block::BlockScope;
use mwapi_responses::{prelude::*, query_api};
use mwbot::parsoid::map::IndexMap;
use mwbot::parsoid::prelude::*;
use mwbot::timestamp::{Expiry, Timestamp};
use mwbot::{Bot, SaveOptions};
use regex::Regex;
use serde::Deserialize;
use tokio::signal::unix::{signal, SignalKind};
use tokio::sync::mpsc;
use tokio::time::{self, sleep};
use tokio::try_join;
use tracing::{debug, error, info, warn, Level};
use tracing_subscriber::filter;
use tracing_subscriber::prelude::__tracing_subscriber_SubscriberExt;
use tracing_subscriber::util::SubscriberInitExt;
use wikimedia_events::recentchange::RecentChange;

#[query(
    list = "logevents",
    leprop = "ids|user|type|title|userid|details|timestamp|comment|parsedcomment|tags",
    lelimit = "max"
)]
struct EventsResponse;

#[query(
    action = "query",
    list = "blocks",
    bkprop = "id|user|userid|by|byid|timestamp|expiry|reason|range|flags|restrictions"
)]
struct BlocksResponse;

#[query(
    action = "query",
    list = "globalblocks",
    bgprop = "id|target|by|timestamp|expiry|reason|range"
)]
struct GlobalBlocksResponse;

enum Never {}

fn is_incomplete_section(section: &Section) -> bool {
    let Some(heading) = section.heading() else {
        return false;
    };
    if heading.level() != 2 {
        return false;
    }
    !Regex::new(r"(?i)\( *(((nicht +)?(erl\.?|erledigt))|in Bearbeitung) *\)")
        .expect("regex should compile")
        .is_match(heading.text_contents().as_str())
}

async fn process_vm_page_debounce(
    bot: Bot,
    meta_bot: Bot,
    s: mpsc::Sender<()>,
    mut r: mpsc::Receiver<()>,
) -> Result<Never> {
    process_vm_page(&bot, &meta_bot, &s).await?;

    let mut process = false;
    loop {
        match time::timeout(Duration::from_secs(5), r.recv()).await {
            Ok(Some(())) => {
                process = true;
            }
            Ok(None) => {
                bail!("process vm page channel was unexpectedly closed")
            }
            Err(_) => {
                // timeout
                if process {
                    process = false;
                    process_vm_page(&bot, &meta_bot, &s).await?;
                }
            }
        }
    }
}

#[derive(Debug, Clone)]
struct BlockDetails {
    target: String,
    timestamp: Timestamp,
    by: String,
    reason: String,
    expiry: Expiry,
    partial: bool,
    scope: BlockScope,
}

#[derive(Debug, Clone)]
enum BlockLockInfo {
    LocalIp(BlockDetails),
    GlobalBlock(BlockDetails),
    GlobalLock(BlockDetails),
}

async fn process_vm_page(bot: &Bot, meta_bot: &Bot, s: &mpsc::Sender<()>) -> Result<()> {
    debug!("Checking VM...");

    #[cfg(debug_assertions)]
    let title = "Benutzer:Count Count/VMTest";
    #[cfg(not(debug_assertions))]
    let title = "Wikipedia:Vandalismusmeldung";

    let vm_page = bot.page(title).context("getting VM page")?;
    let html = vm_page.html().await.context("getting VM page text")?;

    let user_reports = find_open_user_reports(&html);

    if !user_reports.iter().any(|e| e.is_some()) {
        touch_vm_processed_timestamp()?;
        return Ok(());
    }

    let mut blocks = Vec::new();
    for report in &user_reports {
        let Some(report) = report else {
            blocks.push(None);
            continue;
        };
        blocks.push(check_block(report, bot, meta_bot).await?);
    }

    if blocks.iter().all(|e| e.is_none()) {
        touch_vm_processed_timestamp()?;
        return Ok(());
    }

    match vm_page
        .save(
            get_new_vm_page_contents(bot, html, &blocks)?,
            &SaveOptions::summary(get_new_vm_page_edit_summary(&user_reports).as_str())
                .mark_as_bot(true)
                .mark_as_minor(true),
        )
        .await
    {
        Ok((_page, _save_result)) => {
            info!("VM page saved");
        }
        Err(mwbot::Error::EditConflict) => {
            info!("Edit conflict, retrying save...");
            s.send(()).await?;
        }
        Err(mwbot::Error::HttpError(e))
            if e.status()
                .is_some_and(|status| status.as_u16() == http::status::StatusCode::NOT_FOUND) =>
        {
            // TODO: find out why this happens on edit conflicts
            info!("Expected parsoid error (edit conflict), retrying save...");
            s.send(()).await?;
        }
        Err(e) => {
            Err(e)?;
        }
    }
    touch_vm_processed_timestamp()?;
    Ok(())
}

fn touch_vm_processed_timestamp() -> Result<()> {
    let mut temp_dir = env::temp_dir();
    temp_dir.push("vm_processed.timestamp");
    OpenOptions::new()
        .create(true)
        .write(true)
        .truncate(true)
        .open(&temp_dir)?;
    Ok(())
}

fn get_new_vm_page_edit_summary(reports: &Vec<Option<UserReport>>) -> String {
    let mut res = "Bot: ".to_string();
    let mut first = true;
    for report in reports {
        let Some(report) = report else {
            continue;
        };
        if !first {
            write!(res, "; ").unwrap();
        }
        write!(
            res,
            "Abschnitt erledigt: [[Spezial:Beiträge/{report}|{report}]]"
        )
        .unwrap();
        first = false;
    }
    res
}
fn get_new_vm_page_contents(
    bot: &Bot,
    html: ImmutableWikicode,
    open_vm_and_block_info: &Vec<Option<BlockLockInfo>>,
) -> Result<ImmutableWikicode> {
    let mutable_html = html.into_mutable();
    for (section, block_info) in
        iter::zip(mutable_html.iter_sections().iter(), open_vm_and_block_info)
    {
        let Some(block_info) = block_info else {
            continue;
        };

        let (action, block_params) = match block_info {
            BlockLockInfo::LocalIp(block_params) => (None, block_params),
            BlockLockInfo::GlobalBlock(block_params) => (
                Some("[[:m:Global blocks/de|global gesperrt]]"),
                block_params,
            ),
            BlockLockInfo::GlobalLock(block_params) => {
                (Some("[[:m:Global locks/de|global gesperrt]]"), block_params)
            }
        };

        section
            .heading()
            .expect("expected section to have a heading")
            .append(&Wikicode::new_text(" (erl.)"));

        let mut params = IndexMap::new();
        params.insert(
            "Gemeldeter".to_string(),
            format!(
                "[[Spezial:Beiträge/{0}|{0}]]",
                block_params.target.to_string(),
            ),
        );
        let duration = match block_params.expiry {
            Expiry::Infinity => "unbeschränkt".to_string(),
            Expiry::Finite(end) => human_readable_duration(
                (*end - *block_params.timestamp)
                    .num_seconds()
                    .unsigned_abs(),
            ),
        };
        if let Some(action) = action {
            params.insert("Aktion".to_string(), action.to_string());
        }
        params.insert("Admin".to_string(), block_params.by.clone());
        params.insert("Zeit".to_string(), duration);
        params.insert("Begründung".to_string(), block_params.reason.clone());
        params.insert(
            "Teilsperre".to_string(),
            format_restrictions(
                bot,
                action.is_some(),
                block_params.partial,
                block_params.scope.clone(),
            ),
        );
        params.insert("subst".to_string(), "subst:".to_string());
        let template = Template::new("subst:Benutzer:Xqbot/VM-erledigt", &params)?;
        section.append(&template);
    }
    Ok(mutable_html.into_immutable())
}

fn human_readable_duration(seconds: u64) -> String {
    if seconds == 0 {
        return "0 Sekunden".to_string();
    }

    let time_units = [
        (seconds / 86400, "Tag", "Tage"),
        ((seconds % 86400) / 3600, "Stunde", "Stunden"),
        ((seconds % 3600) / 60, "Minute", "Minuten"),
        (seconds % 60, "Sekunde", "Sekunden"),
    ];

    let mut res = String::new();
    for (v, s, p) in time_units {
        if v > 0 {
            if !res.is_empty() {
                res.push_str(", ");
            }
            write!(&mut res, "{} {}", v, if v == 1 { s } else { p }).unwrap();
        }
    }
    res
}

fn format_restrictions(bot: &Bot, global: bool, partial: bool, scope: BlockScope) -> String {
    if global {
        return "in allen Wikimedia-Projekten".to_owned();
    }

    if !partial {
        return String::new();
    }

    let mut res = "für ".to_owned();
    let restr = match scope {
        BlockScope::Sitewide(_) => "bestimmte Aktionen".to_owned(),
        BlockScope::Partial(restrictions) => {
            let mut vec = Vec::with_capacity(2);
            if !restrictions.namespaces.is_empty() {
                let prefix = if restrictions.namespaces.len() == 1 {
                    "den Namensraum"
                } else {
                    "die Namensräume"
                };
                let fmt = restrictions
                    .namespaces
                    .iter()
                    .map(|ns| {
                        if *ns == 0 {
                            "(Artikel)"
                        } else {
                            bot.namespace_name(*ns).expect("namespace should exist")
                        }
                    })
                    .format(", ");
                vec.push(format!("{prefix} {fmt}"));
            }
            if !restrictions.pages.is_empty() {
                let prefix = if restrictions.pages.len() == 1 {
                    "die Seite"
                } else {
                    "die Seiten"
                };
                let fmt = restrictions
                    .pages
                    .iter()
                    .map(|page| format!("[[{}]]", page.title))
                    .join(", ");
                vec.push(format!("{prefix} {fmt}"));
            }
            vec.iter().format(" und ").to_string()
        }
    };
    res.push_str(restr.as_str());
    res
}

async fn check_block(
    report: &UserReport,
    bot: &Bot,
    meta_bot: &Bot,
) -> Result<Option<BlockLockInfo>> {
    debug!("Checking blocks for {report}");

    if let Some(bi) = check_local_blocks(bot, report).await? {
        return Ok(Some(bi));
    }

    if let Some(bi) = check_global_blocks(meta_bot, report).await? {
        return Ok(Some(bi));
    }

    if let Some(bi) = check_global_locks(meta_bot, report).await? {
        return Ok(Some(bi));
    }

    Ok(None)
}

async fn check_local_blocks(bot: &Bot, report: &UserReport) -> Result<Option<BlockLockInfo>> {
    let UserReport::Ip(inet) = &report else {
        return Ok(None);
    };

    let blocks =
        mwapi_responses::query_api::<BlocksResponse, _, _>(bot.api(), [("bkip", inet.to_string())])
            .await
            .context(std::format!("querying range blocks for {inet}"))?;
    for block in blocks.into_items() {
        if block.partial && Utc::now() - *block.timestamp > TimeDelta::seconds(15 * 60) {
            continue;
        }
        if block.rangestart == block.rangeend {
            continue;
        }
        let (Some(range_start), Some(range_end)) = (block.rangestart, block.rangeend) else {
            continue;
        };
        let cidr = match (&inet, range_start, range_end) {
            (IpInet::V4(_), std::net::IpAddr::V4(start), std::net::IpAddr::V4(end)) => {
                #[allow(clippy::cast_possible_truncation)]
                let len = start.shared_prefix_len(&end, 32) as u8;
                IpCidr::V4(Ipv4Cidr::new(start, len).context(format!(
                    "creating CIDR from IPv4 range {range_start} - {range_end}"
                ))?)
            }
            (IpInet::V6(_), std::net::IpAddr::V6(start), std::net::IpAddr::V6(end)) => {
                #[allow(clippy::cast_possible_truncation)]
                let len = start.shared_prefix_len(&end, 128) as u8;
                IpCidr::V6(Ipv6Cidr::new(start, len).context(format!(
                    "creating CIDR from IPv6 range {range_start} - {range_end}"
                ))?)
            }
            _ => bail!("Incompatible IP range {inet}: {range_start} - {range_end} for {inet}"),
        };
        info!("New relevant range block found: {inet} blocked via {cidr}");
        return Ok(Some(BlockLockInfo::LocalIp(BlockDetails {
            target: cidr.to_string(),
            timestamp: block.timestamp,
            by: block.by,
            reason: block.reason,
            expiry: block.expiry,
            partial: block.partial,
            scope: block.restrictions,
        })));
    }
    Ok(None)
}

async fn check_global_blocks(bot: &Bot, report: &UserReport) -> Result<Option<BlockLockInfo>> {
    let params = match report {
        UserReport::Ip(inet) => [("bgip", inet.to_string())],
        UserReport::User(user) => [("bgtargets", user.clone())],
    };
    let blocks = mwapi_responses::query_api::<GlobalBlocksResponse, _, _>(bot.api(), params)
        .await
        .context(std::format!("querying global range blocks for {report}"))?;
    let Some(block) = blocks.into_items().next() else {
        return Ok(None);
    };
    info!(
        "New relevant global block found: {report} blocked via {}",
        block.target
    );
    Ok(Some(BlockLockInfo::GlobalBlock(BlockDetails {
        target: block.target,
        timestamp: block.timestamp,
        by: block.by,
        reason: block.reason,
        expiry: block.expiry,
        partial: false,
        scope: BlockScope::Sitewide([(); 0]),
    })))
}

async fn check_global_locks(meta_bot: &Bot, report: &UserReport) -> Result<Option<BlockLockInfo>> {
    let UserReport::User(user) = &report else {
        return Ok(None);
    };
    let mw_client = meta_bot.api();

    let Some(ev) = query_api::<EventsResponse, _, _>(
        mw_client,
        [
            ("leaction", "globalauth/setstatus".to_string()),
            ("lelimit", "1".to_string()),
            ("letitle", format!("User:{user}@global").to_string()),
        ],
    )
    .await?
    .into_items()
    .next() else {
        return Ok(None);
    };

    #[derive(Debug, Deserialize)]
    struct SetStatusParams {
        added: Vec<String>,
        removed: Vec<String>,
    }
    let params =
        serde_json::from_value::<SetStatusParams>(ev.params).context("parsing event params")?;
    if params.added.contains(&"locked".to_string())
        && !params.removed.contains(&"locked".to_string())
    {
        info!("New relevant global lock found: {user}");
        return Ok(Some(BlockLockInfo::GlobalLock(BlockDetails {
            target: user.clone(),
            timestamp: ev.timestamp,
            by: ev.user,
            reason: ev.parsedcomment,
            expiry: Expiry::Infinity,
            partial: false,
            scope: BlockScope::Sitewide([(); 0]),
        })));
    }
    Ok(None)
}

enum UserReport {
    Ip(IpInet),
    User(String),
}

impl Display for UserReport {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Ip(ip) => write!(f, "{ip}"),
            Self::User(user) => write!(f, "{user}"),
        }
    }
}

fn get_user_report(section: &Section) -> Option<UserReport> {
    if !is_incomplete_section(section) {
        return None;
    }
    let links = section.heading()?.filter_links();
    let [ref only_link] = *links else {
        return None;
    };
    let target = only_link.target();
    let user_page = target.strip_prefix("Benutzer:")?.trim();
    if let Ok(ip) = IpInet::from_str(user_page.trim()) {
        return Some(UserReport::Ip(ip));
    }
    Some(UserReport::User(user_page.to_string()))
}

fn find_open_user_reports(html: &ImmutableWikicode) -> Vec<Option<UserReport>> {
    let mut res = Vec::new();
    for section in html.clone().into_mutable().iter_sections() {
        res.push(get_user_report(&section));
    }
    res
}

fn seconds_since_epoch() -> u64 {
    SystemTime::now()
        .duration_since(SystemTime::UNIX_EPOCH)
        .expect("should be able to calculate duration since epoch")
        .as_secs()
}

async fn regularly_check_vm(s: mpsc::Sender<()>) -> Result<Never> {
    loop {
        debug!("Triggering regular check.");
        s.send(()).await?;
        sleep(Duration::from_secs(60)).await;
    }
}

async fn handle_vm_page_changes(s: mpsc::Sender<()>) -> Result<Never> {
    let mut stream = wikimedia_eventstreams::Builder::new()
        .build::<RecentChange>()
        .context("building sse client")?;

    loop {
        let event = match stream.try_next().await {
            Ok(Some(event_or_comment)) => event_or_comment,
            Ok(None) => {
                bail!("Should not happen");
            }
            Err(err) => return Err(anyhow::Error::from(err)).context("getting next event"),
        };
        let event_data = event.data;

        if !matches!(event_data.wiki.as_deref(), Some("dewiki")) {
            continue;
        }

        if matches!(event_data.bot, Some(true)) {
            continue;
        }

        if matches!(
            event_data.title.as_deref(),
            Some("Wikipedia:Vandalismusmeldung")
        ) {
            debug!("VM change found.");
            s.send(()).await?;
        }
    }
}

async fn handle_block_log(bot: Bot, s: mpsc::Sender<()>) -> Result<Never> {
    let mw_client = bot.api();
    let mut last_timestamp = seconds_since_epoch();
    loop {
        let new_timestamp = seconds_since_epoch();
        debug!("Getting blocks");
        match query_api::<EventsResponse, _, _>(
            mw_client,
            [
                ("leend", last_timestamp.to_string()),
                ("letype", "block".to_string()),
            ],
        )
        .await
        {
            Ok(block_events) => {
                for block_event in block_events.items() {
                    if !["block", "reblock"].contains(&block_event.action.as_str()) {
                        continue;
                    }
                    let Some(ref block_event_title) = block_event.title else {
                        continue;
                    };
                    if !block_event_title.starts_with("Benutzer:") {
                        bail!(
                            "Block title not starting with 'Benutzer' - {}",
                            block_event_title
                        );
                    }
                    let Ok(cidr) = IpCidr::from_str(&block_event_title[9..]) else {
                        continue;
                    };
                    if cidr.is_host_address() {
                        continue;
                    }
                    debug!("At least one new range block found: {}", cidr);
                    s.send(()).await?;
                    break;
                }
                last_timestamp = new_timestamp;
                sleep(Duration::from_secs(10)).await;
            }
            Err(e) => Err(e).context("getting blocks")?,
        }
    }
}

async fn handle_global_block_log(meta_bot: Bot, s: mpsc::Sender<()>) -> Result<Never> {
    let mw_client = meta_bot.api();
    let mut last_timestamp = seconds_since_epoch();
    loop {
        let new_timestamp = seconds_since_epoch();
        match query_api::<EventsResponse, _, _>(
            mw_client,
            [
                ("leend", last_timestamp.to_string()),
                ("letype", "gblblock".to_string()),
            ],
        )
        .await
        {
            Ok(block_events) => {
                for block_event in block_events.items() {
                    if !["gblock", "greblock"].contains(&block_event.action.as_str()) {
                        continue;
                    }
                    let Some(ref block_event_title) = block_event.title else {
                        continue;
                    };
                    if !block_event_title.starts_with("User:") {
                        bail!(
                            "Global block title not starting with 'User' - {}",
                            block_event_title
                        );
                    }
                    let target = &block_event_title[5..];
                    debug!("At least one new global block found: {}", target);
                    s.send(()).await?;
                    break;
                }
                last_timestamp = new_timestamp;
                sleep(Duration::from_secs(10)).await;
            }
            Err(e) => Err(e).context("getting global blocks")?,
        }
    }
}

async fn handle_global_auth_log(meta_bot: Bot, s: mpsc::Sender<()>) -> Result<Never> {
    let mw_client = meta_bot.api();
    let mut last_timestamp = seconds_since_epoch();
    loop {
        let new_timestamp = seconds_since_epoch();
        match query_api::<EventsResponse, _, _>(
            mw_client,
            [
                ("leend", last_timestamp.to_string()),
                ("leaction", "globalauth/setstatus".to_string()),
            ],
        )
        .await
        {
            Ok(block_events) => {
                if block_events.items().next().is_some() {
                    debug!("At least one new global auth setstatus event found");
                    s.send(()).await?;
                }
                last_timestamp = new_timestamp;
                sleep(Duration::from_secs(10)).await;
            }
            Err(e) => Err(e).context("getting global auth events")?,
        }
    }
}

async fn create_dewiki_bot() -> Result<Bot> {
    if let Ok(tdd) = env::var("TOOL_DATA_DIR") {
        let mut buf = PathBuf::from(&tdd);
        buf.push("mwbot.toml");
        Bot::from_path(&buf).await
    } else {
        Bot::from_default_config().await
    }
    .context("creating mwbot")
}

async fn create_meta_bot() -> Result<Bot> {
    let bot = Bot::builder("https://meta.wikimedia.org/w/".to_string())
        .build()
        .await?;
    Ok(bot)
}

// suppress macro-related clippy warnings
#[allow(clippy::redundant_pub_crate)]
#[allow(unreachable_code)]
async fn run() -> Result<()> {
    let (s, r) = mpsc::channel(1000);
    let bot = create_dewiki_bot().await?;
    let meta_bot = create_meta_bot().await?;
    let mut sigint = signal(SignalKind::interrupt())?;
    let mut sigterm = signal(SignalKind::terminate())?;
    tokio::select! {
        r = async {
            try_join!(
                handle_global_block_log(meta_bot.clone(), s.clone())
                    .map(|r| r.context("handling global block log")),
                handle_global_auth_log(meta_bot.clone(), s.clone())
                    .map(|r| r.context("handling global auth log")),
                handle_block_log(bot.clone(), s.clone()).map(|r| r.context("handling block log")),
                handle_vm_page_changes(s.clone()).map(|r| r.context("handling VM page changes")),
                regularly_check_vm(s.clone()).map(|r| r.context("regular VM check")),
                process_vm_page_debounce(bot.clone(), meta_bot.clone(), s.clone(), r)
                    .map(|r| r.context("process vm page debounce"))
        )} => { r?; },
        _ = sigint.recv() => { warn!("Received SIGINT - exiting..."); return Ok(());},
        _ = sigterm.recv() => { warn!("Received SIGTERM - exiting..."); return Ok(()) },
    }
    unreachable!();
}

fn init_logging() {
    let filter = filter::Targets::new()
        .with_default(Level::INFO)
        .with_target("eventsource_client::client", Level::WARN);

    tracing_subscriber::registry()
        .with(tracing_subscriber::fmt::layer())
        .with(filter)
        .init();
}

#[tokio::main(flavor = "current_thread")]
async fn main() {
    init_logging();
    info!("Starting bot");
    #[allow(clippy::large_futures)]
    if let Err(err) = run().await {
        error!("Error: {:#}, exiting...", err);
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use mwapi_responses::block::{BlockScope, PageBlockRestriction, PartialBlockRestrictions};
    use mwbot::Bot;

    fn assert_restriction_formatting(bot: &Bot, scope: (&[i32], &[(i32, &str)]), expected: &str) {
        let scope = BlockScope::Partial(PartialBlockRestrictions {
            namespaces: scope.0.to_owned(),
            pages: scope
                .1
                .iter()
                .map(|page| PageBlockRestriction {
                    id: 0,
                    ns: page.0,
                    title: page.1.to_owned(),
                })
                .collect(),
        });
        assert_eq!(
            format_restrictions(bot, false, true, scope).as_str(),
            expected
        );
    }

    #[tokio::test]
    async fn test_restrictions_formatting() {
        let bot = Bot::builder("https://de.wikipedia.org/w/".to_string())
            .build()
            .await
            .expect("bot creation should work");
        assert_eq!(
            format_restrictions(&bot, false, false, BlockScope::Sitewide([(); 0])).as_str(),
            ""
        );
        assert_eq!(
            format_restrictions(&bot, true, false, BlockScope::Sitewide([(); 0])).as_str(),
            "in allen Wikimedia-Projekten"
        );
        assert_eq!(
            format_restrictions(&bot, false, true, BlockScope::Sitewide([(); 0])).as_str(),
            "für bestimmte Aktionen"
        );
        [
            ((vec![0], vec![]), "für den Namensraum (Artikel)"),
            (
                (vec![0, 1], vec![]),
                "für die Namensräume (Artikel), Diskussion",
            ),
            (
                (vec![], vec![(0, "Albert Einstein")]),
                "für die Seite [[Albert Einstein]]",
            ),
            (
                (vec![], vec![(0, "Albert Einstein"), (1, "Diskussion:Galileo Galilei")]),
                "für die Seiten [[Albert Einstein]], [[Diskussion:Galileo Galilei]]",
            ),
            (
                (vec![0, 1], vec![(0, "Albert Einstein"), (1, "Diskussion:Galileo Galilei")]),
                "für die Namensräume (Artikel), Diskussion und die Seiten [[Albert Einstein]], [[Diskussion:Galileo Galilei]]",
            ),
        ]
        .map(|(params, expected)|
            assert_restriction_formatting(&bot, (&params.0, &params.1), expected)
        );
    }

    fn assert_duration_human_readable(secs: u64, expected: &str) {
        assert_eq!(human_readable_duration(secs), expected);
    }

    #[test]
    fn test_human_readable_duration() {
        [
            (0, "0 Sekunden"),
            (1, "1 Sekunde"),
            (60, "1 Minute"),
            (122, "2 Minuten, 2 Sekunden"),
            (
                60 * 60 * 24 + 2 * 60 * 60 + 3 * 60 + 4,
                "1 Tag, 2 Stunden, 3 Minuten, 4 Sekunden",
            ),
        ]
        .map(|(secs, s)| assert_duration_human_readable(secs, s));
    }

    #[tokio::test]
    async fn test_global_block_ip() {
        let bot = Bot::builder("https://de.wikipedia.org/w/".to_string())
            .build()
            .await
            .expect("bot creation should work");
        check_global_blocks(
            &bot,
            &UserReport::Ip(IpInet::from_str("126.167.0.0/16").unwrap()),
        )
        .await
        .unwrap()
        .unwrap();
    }

    #[tokio::test]
    async fn test_global_block_user() {
        let bot = Bot::builder("https://de.wikipedia.org/w/".to_string())
            .build()
            .await
            .expect("bot creation should work");
        let block = check_global_blocks(&bot, &UserReport::User("Bha.p".to_string()))
            .await
            .unwrap()
            .unwrap();
        let BlockLockInfo::GlobalBlock(block) = block else {
            panic!("expected global block");
        };
        assert!(block.expiry.is_infinity());
    }

    #[tokio::test]
    async fn test_global_lock() {
        let bot = Bot::builder("https://meta.wikimedia.org/w/".to_string())
            .build()
            .await
            .expect("bot creation should work");
        let block = check_global_locks(&bot, &UserReport::User("KeithIceft".to_string()))
            .await
            .unwrap()
            .unwrap();
        let BlockLockInfo::GlobalLock(block) = block else {
            panic!("expected global lock");
        };
        assert!(block.expiry.is_infinity());
    }
}
