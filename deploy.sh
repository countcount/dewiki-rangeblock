#!/usr/bin/env bash
set -euo pipefail
ssh dev.toolforge.org 'set -euo pipefail
echo Building...
become dewiki-rangeblock toolforge build start https://gitlab.wikimedia.org/countcount/dewiki-rangeblock.git
echo Restarting...
become dewiki-rangeblock toolforge jobs restart dewiki-rangeblock
echo Done.'
